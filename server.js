const express = require('express')
const axios = require('axios')
const js2xmlparser = require('js2xmlparser')

const app = express()
const port = 3000

app.get('/:estado/:cidade/:logradouro/:tipo', (req, res, next) => {
  const { estado, cidade, logradouro, tipo } = req.params
  res.locals.requestInfos = { estado, cidade, logradouro, tipo }
  next()
})

// middlewares to request
app.use(async (_req, res, next) => {
  const { estado, cidade, logradouro } = res.locals.requestInfos

  const { data } = await axios.get(
    `https://viacep.com.br/ws/${estado}/${cidade}/${logradouro}/json/`
  )
  res.locals.requestDataJson = data
  next()
})

// convert json to xml
app.use((_req, res, next) => {
  const { tipo } = res.locals.requestInfos

  if (tipo === 'xml') {
    res.locals.xml = js2xmlparser.parse(
      'xmlPostalCode',
      res.locals.requestDataJson
    )
  }

  next()
})

// return xml
app.use((_req, res, next) => {
  const { tipo } = res.locals.requestInfos

  if (tipo === 'xml') {
    return res.send(res.locals.xml)
  }

  next()
})

// return json
app.use((_req, res, next) => {
  const { tipo } = res.locals.requestInfos

  if (tipo === 'json') {
    return res.send(res.locals.requestDataJson)
  }

  next()
})

app.listen(port, () => {})
